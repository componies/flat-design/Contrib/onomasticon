(($) => {
  'use strict';

  Drupal.behaviors.onomasticon = {
    attach: function(context, settings) {
      $('[data-tooltip]', context).once('enable_tooltips').each(function() {
        let $trigger = $(this);
        let verticalPosition = 'top';

        $trigger.on("mouseleave", function (e) {
          e.stopImmediatePropagation();
        });

        $trigger.on("mouseenter", function (e) {
          e.stopImmediatePropagation();
        });

        $trigger.on("focusin", function (e) {
          e.stopImmediatePropagation();
        });

        $trigger.on("mouseover", function (e) {
          e.stopImmediatePropagation();
        });

        $trigger.tooltip({
          items: "[data-tooltip]",
          show: { effect: "fadeToggle", duration: 400, easing: "linear" },
          hide: { effect: "fadeToggle", duration: 400, easing: "linear" },
          content: function() {
            let tooltip = $trigger.data("tooltip");
            let title = $trigger.text();
            return '<p><b>' + title.charAt(0).toUpperCase() + title.slice(1) + '</b></br>' + tooltip + '</p>';
          },
          position: {
            my: "left-32 bottom-20",
            at: "left top",
            using: function(position, feedback) {
              $(this).css(position);
              verticalPosition = feedback.vertical;
              $("<div>")
                .addClass("arrow")
                .addClass(feedback.vertical)
                .addClass(feedback.horizontal)
                .appendTo(this);
            }
          }
        }).on("tooltipopen", function( event, ui ) {
          $trigger.removeClass('tooltip-bottom');
          $trigger.removeClass('tooltip-top');
          $trigger.addClass('tooltipped');
          $trigger.addClass('tooltip-' + verticalPosition);
        }).on("tooltipclose", function( event, ui ) {
          $trigger.removeClass('tooltipped');
        });

        $trigger.on('click', function (e) {
          e.preventDefault();
          e.stopPropagation();
          if($trigger.hasClass('tooltipped')) {
            $trigger.tooltip('close');
          } else {
            $trigger.tooltip('open');
          }
        });
      });
    }
  };
})(jQuery);
